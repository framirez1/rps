# README #

This is my take on the typical Rock Paper Scissors game. The game is played via the console, and the user needs to choose a strategy for the computer to use.

### Usage ###

$ python3 rps.py -s [strategy type]

### Set Up ###

* Clone the repository
    * Note: The game was developed in Visual Studio, but running the game does not require Visual Studio.
* Once the repository has been cloned, running it is easy:

~~~~
 $ cd ./RockpaperScissorsGame/
 $ python3 rds.py -s favorite
   Welcome to Rock, Paper, Scissors!
   Type one of the following: s, r, p

~~~~

* To run unit tests:
~~~~
 $ python3 -m unittest discover -p *tests.py

~~~~