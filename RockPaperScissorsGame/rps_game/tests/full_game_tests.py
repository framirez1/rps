import unittest

from ..engine import rps_game as rps
from ..engine import strategies as s

class Test_full_game_tests(unittest.TestCase):
    
   
   def test_full_game_human_vs_ai(self):
      
      from io import StringIO

      game = rps.RpsGame()

      output = StringIO()

      with open("./rps_game/tests/test_input/user_input.txt") as r:
         game.start(s.FavoriteStrategy(), input_func = lambda: r.readline(), output_func = lambda x : output.write(x + '\n'))
         output.flush()
      
      lines = output.getvalue()

      output.close()

      expected_output = '''Welcome to Rock, Paper, Scissors!
I chose p. I win!
you won 0 times. 
you lost 1 times. 
we tied 0 times.
I chose p. I win!
you won 0 times. 
you lost 2 times. 
we tied 0 times.
I chose p. It's a tie!
you won 0 times. 
you lost 2 times. 
we tied 1 times.
Game is exiting, final stats:
you won 0 times. 
you lost 2 times. 
we tied 1 times.
'''

      self.assertTrue(expected_output == lines)

if __name__ == '__main__':
    unittest.main()
