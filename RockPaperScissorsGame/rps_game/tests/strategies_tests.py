import unittest

from ..engine import player as p
from ..engine import moves as m
from ..engine import strategies as s

class Test_strategies_tests(unittest.TestCase):
   
   def test_favorite_strategy(self):
      """Tests the 'favorite move' strategy.
      """
      p1 = p.HumanPlayer();

      p1._add_move(m.Move.paper);
      p1._add_move(m.Move.scissors);
      p1._add_move(m.Move.rock);
      p1._add_move(m.Move.paper);

      p2 = p.AiPlayer(strategy = s.FavoriteStrategy())

      move = p2.execute_turn(p1);

      self.assertEqual(m.Move.scissors, move)

   def test_last_move_strategy(self):
      """Tests the 'last move' strategy.
      """
      p1 = p.HumanPlayer();

      p1._add_move(m.Move.rock);

      p2 = p.AiPlayer(strategy = s.LastMoveStrategy())

      move = p2.execute_turn(p1);

      self.assertEqual(m.Move.paper, move)

   def test_random_move_strategy(self):
      """Test the 'random-move' strategy.
      """
      p1 = p.AiPlayer(strategy = s.RandomMoveStrategy(seed_value = 1234));

      move = p1.execute_turn(human_player = p.HumanPlayer())

      self.assertEqual(m.Move.rock, move)

   def test_strategy_factory(self):
      """Test the strategy factory.
      """

      self.assertEqual(type(s.strategy_factory(s.available_types[0])), type(s.FavoriteStrategy()))

      self.assertEqual(type(s.strategy_factory(s.available_types[1])), type(s.LastMoveStrategy()))

      self.assertEqual(type(s.strategy_factory(s.available_types[2])), type(s.RandomMoveStrategy()))

      self.assertEqual(s.strategy_factory('foobar'), None)

      
    

