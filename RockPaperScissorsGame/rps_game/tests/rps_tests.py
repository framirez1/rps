import unittest

from ..engine import moves as m

class Test_rbs_tests(unittest.TestCase):
   
   def test_determine_winner(self):
      """Test the 'determine_winner' function.
      """

      self.assertEqual(m.Move.rock, m.determine_winner(m.Move.rock, m.Move.scissors))

      self.assertEqual(m.Move.paper, m.determine_winner(m.Move.paper, m.Move.rock))

      self.assertEqual(m.Move.scissors, m.determine_winner(m.Move.paper, m.Move.scissors))

      self.assertEqual(None, m.determine_winner(m.Move.paper, m.Move.paper))


   def test_get_superior_move(self):
      """Test the 'get_superior_move' function.
      """

      self.assertEqual(m.Move.rock, m.get_superior_move(m.Move.scissors))

      self.assertEqual(m.Move.paper, m.get_superior_move(m.Move.rock))

      self.assertEqual(m.Move.scissors, m.get_superior_move(m.Move.paper))

   

