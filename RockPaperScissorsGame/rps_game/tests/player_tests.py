import unittest

from ..engine import player as p
from ..engine import moves as m
from ..engine import game_stats as g

class Test_player_tests(unittest.TestCase):

   def test_initialize_player(self):
      """Test basic initialization of Player class.
      """
      p1 = p.Player("bob")

      self.assertEqual(p1.name, "bob")

      self.assertEqual(repr(p1._game_stats),"GameStats(wins=0, losses=0, ties=0)" )

   def test_add_move_verify_turn_stats(self):
      """Test additions of all moves.

      This test verifies that all moves can be added to the 
      history of the player.
      """
      p1 = p.Player("Jim")

      p1._add_move(m.Move.paper)
      p1._add_move(m.Move.rock)
      p1._add_move(m.Move.scissors)

      moves = {m.Move.paper : 1,
               m.Move.rock  : 1,
               m.Move.scissors : 1}

      self.assertDictEqual(p1._turn_statistics, moves)

      self.assertEqual(len(p1._turns_played), 3)

   def test_add_multiple_moves_verify_turn_stats(self):
      """Test multiple additions of the same move.

      This test verifies that sequential moves of the same type are
      appropriately added together.
      """
      p1 = p.Player("John")

      p1._add_move(m.Move.paper)
      p1._add_move(m.Move.paper)
      p1._add_move(m.Move.paper)
      p1._add_move(m.Move.paper)

      moves = {m.Move.paper : 4}

      self.assertDictEqual(p1._turn_statistics, moves)

      self.assertEqual(len(p1._turns_played), 4)

   def test_add_move_verify_cache_limit(self):
      """Test that the cache limit is honored.
      """

      from ..engine import config as c

      p1 = p.Player("Ted")

      for _ in range(c.MAX_CACHED_TURNS + 2):
         p1._add_move(m.Move.rock)

      self.assertEqual(len(p1._turns_played), 10)

   def test_verify_game_stats(self):
      """Test that the game stats are updated appropriately.
      """

      p1 = p.Player()

      p1.record_win()
      p1.record_loss()
      p1.record_win()

      stats = g.GameStats(wins=2, losses=1)

      self.assertEqual(repr(p1._game_stats), repr(stats))

   def test_most_frequent_move(self):
      """Test that we are able to retrieve the most frequent move.
      """
      p1 = p.Player()

      p1._add_move(m.Move.paper)
      p1._add_move(m.Move.paper)
      p1._add_move(m.Move.rock)
      p1._add_move(m.Move.scissors)

      self.assertEqual(p1.most_frequent_move, m.Move.paper)

      p1._add_move(m.Move.scissors)
      p1._add_move(m.Move.scissors)

      self.assertEqual(p1.most_frequent_move, m.Move.scissors)

   def test_human_input_logic_good_input(self):
      """Tests that the human input is parsed correctly.
      """
      p1 = p.HumanPlayer("Bob");

      move = None

      with open("./rps_game/tests/test_input/valid.txt") as r:
         move = p1.execute_turn(lambda: r.readline())

      self.assertEqual(move, m.Move.rock)

   def test_human_input_logic_bad_input(self):
      """Tests that invalid input can be entered wihtout crashing.

      The test case eventually provides valid input.
      """
      p1 = p.HumanPlayer("Bob");

      move = None

      with open("./rps_game/tests/test_input/invalid.txt") as r:
         move = p1.execute_turn(lambda: r.readline())

      self.assertEqual(move, m.Move.paper)