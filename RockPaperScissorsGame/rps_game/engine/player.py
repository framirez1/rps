
from enum import Enum
from collections import deque

from .config import MAX_CACHED_TURNS
from .game_stats import GameStats
from .strategies import FavoriteStrategy, LastMoveStrategy
from .moves import Move

class Player(object):
   """A player of the rock paper scissors game.   

   Attributes:
      name (str): The name of the player.
      game_stats (GameStats): The statistics of the games played (wins, losses, etc.)

   """

   def __init__(self, name = "Base"):
      """Initializes a new instance of the base player class.
      """
      self._name = name
      self._game_stats  = GameStats()

      self._turn_statistics = {}
      self._turns_played = deque()

   def _add_move(self, move):
      """Adds a move to the players history.

      Args:
        move (Move): The move to add to the players history.
      """
      # Keep the last MAX_CACHED_TURNS in our queue
      if len(self._turns_played) > 0 and len(self._turns_played) >= MAX_CACHED_TURNS:
         self._turns_played.pop();

      self._turns_played.append(move)

      # Keep count of how many times the player has used 
      # each move.
      if move in self._turn_statistics:
         self._turn_statistics[move] += 1
      else:
         self._turn_statistics[move]  = 1

   def record_win(self):
      """Records a win for the player.
      """
      self._game_stats.wins += 1

   def record_loss(self):
      """Records a loss for the player.
      """
      self._game_stats.losses += 1

   def record_tie(self):
      """Records a tie for the player.
      """
      self._game_stats.ties += 1

   def report_game_stats(self):
      """Returns a string representation of the player's statistics.

      The string will be returned in the following format:
       
      'you won 2 times. 
       you lost 1 times. 
       we tied 2 times.'

      """
      return self._game_stats.report_basic_stats()

   def execute_turn(self):
      pass

   @property
   def most_frequent_move(self):
      """Returns the players most frequent move.
      """
      return max(self._turn_statistics, key=self._turn_statistics.get) 

   @property
   def last_move(self):
      """Returns the players last move.
      """
      return self._turns_played[-1]

   @property
   def name(self):
      return self._name


class HumanPlayer(Player):

   def __init__(self, name = "Dave"):
      return super().__init__(name)

   def execute_turn(self, input_funct = input, output_func = print):
      """Ask the user for their turn.

      If invalid input is provided, an error message will pop up
      and ask the player to try again.

      Returns one of the available moves, or None if the player 
      wants to quit the game.
      """
      valid_input = False

      options = {name[0][0]:name[1] for name in Move.__members__.items()}

      prompt = ', '.join(options.keys())

      final_move = None

      terminate = 'exit'

      while not valid_input:

         output_func("Type one of the following: {0}, or '{1}' to quit.".format(prompt, terminate))

         response = input_funct().strip()

         if response == terminate:
            output_func("Are you sure you want to quit? (y/n)")
            response = input_funct().strip()

            if response is 'y':
               return None
            else:
               continue

         if response not in options.keys():
            valid_input = False
            output_func("Invalid input, please try again.")
         else:
            final_move = options[response]
            valid_input = True

      self._add_move(final_move)

      return final_move

class AiPlayer(Player):

   def __init__(self, name = "HAL 9000", strategy = FavoriteStrategy()):
      self._strategy = strategy;
      return super().__init__(name)

   def execute_turn(self, human_player):
      """Executes a turn depending on the chosen strategy.
      """

      final_move = self._strategy.compute_turn(human_player);

      self._add_move(final_move)

      return final_move