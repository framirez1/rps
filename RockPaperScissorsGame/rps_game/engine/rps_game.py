
from .player import HumanPlayer, AiPlayer
from .moves  import determine_winner
from .config import PROMPT_FOR_NAME

class RpsGame(object):
   """The game of Rock Paper Scissors.
    
   This is your typical rock, paper, scissors game; where rock 
   beats scissors, scissors beats paper, and paper beats rock.
    
   """

   def _continue_prompt(self, input_funct = input):
      """Prompt the player if they would like to continue.
      """
      raise NotImplementedError("This feature not yet implemented.")

   def _number_of_games_prompt(self, input_funct = input):
      """Prompt the player to enter how many games they would like to play.
      """
      raise NotImplementedError("This feature not yet implemented.")

   def start(self, strategy, input_func = input, output_func = print):
      """Main entry point for the game.
      """

      output_func("Welcome to Rock, Paper, Scissors!")

      name = ""

      if PROMPT_FOR_NAME:
         print("What is your name?")
         name = input()

      player1 = HumanPlayer()

      player2 = AiPlayer(strategy = strategy)

      while True:

         p1_move = player1.execute_turn(input_func)

         if p1_move is None:
            output_func("Game is exiting, final stats:")
            output_func(player1.report_game_stats())
            return

         p2_move = player2.execute_turn(player1)

         winner = determine_winner(p1_move, p2_move)

         if p1_move == winner:
            output_func("I chose {0}. You win!".format(p2_move.name[0]))
            player1.record_win()
            player2.record_loss()
         elif p2_move == winner:
            output_func("I chose {0}. I win!".format(p2_move.name[0]))
            player1.record_loss()
            player2.record_win()
         else:
            output_func("I chose {0}. It's a tie!".format(p2_move.name[0]))
            player1.record_tie()
            player2.record_tie()

         output_func(player1.report_game_stats())

