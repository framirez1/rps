
from enum import Enum, unique

@unique
class Move(Enum):
   """An enum type with all the moves in the game.
   
   Currently the enum contains 3 moves: rock, paper, and scissors
   but this could later be expanded. The enum values follow a pattern
   where the larger number wins over the smaller number (except for 
   the first and last entries).
   """

   # Expanding this enum would require some updates to the various functions
   # in this module.
   scissors = 0
   rock     = 1
   paper    = 2

def determine_winner(move_a, move_b):
   """Attempts to determine a winner between two moves.

   Returns None if results in a tie.
   """

   if type(move_a) is not Move:
      raise TypeError("{0:r} is not of type {1}.".format(move_a, Move))

   if type(move_b) is not Move:
      raise TypeError("{0:r} is not of type {1}.".format(move_b, Move))
   
   if move_a == move_b:
      return None

   diff = (move_a.value - move_b.value)

   # To determine the winner, let's focus on move_a: 
   # does that move win or lose? 
   # if a:scissors and b:rock  - a loses (0 - 1) = -1
   # if a:scissors and b:paper - a wins  (0 - 2) = -2
   # similarly
   # if a:paper and b:rock     - a wins  (2 - 1) =  1
   # if a:paper and b:scissors - a loses (2 - 0) =  2
   # similarly
   # if a:rock and b:paper     - a loses (1 - 2) = -1
   # if a:rock and b:scissors  - a wins  (1 - 0) =  1
   # So a wins when the diff is 1 or -2
   if diff == 1 or diff == -2:
      return move_a
   else:
      return move_b

def get_superior_move(move_to_beat):
   """Gets the move that will beat the argument move.
   """

   if move_to_beat == Move.paper:
      return Move.scissors
   elif move_to_beat == Move.rock:
      return Move.paper
   elif move_to_beat == Move.scissors:
      return Move.rock
   else:
      raise Exception("Enum not recognized.")
    

