
class GameStats(object):
   """Contains the statistics of a game.
   """

   def __init__(self, wins = 0, losses = 0, ties = 0):

      self.wins   = wins
      self.losses = losses
      self.ties   = ties

   def report_basic_stats(self):
      return  "you won {0.wins!r} times. \nyou lost {0.losses!r} times. \nwe tied {0.ties!r} times.".format(self)

   def __repr__(self):
      return 'GameStats(wins={0.wins!r}, losses={0.losses!r}, ties={0.ties!r})'.format(self)
      