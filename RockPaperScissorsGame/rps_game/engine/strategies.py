
from .moves import get_superior_move

available_types = ['favorite', 'last-move', 'random-move']

class StrategyBase(object):
   """The base class for all the game strategies.
   """
   
   def compute_turn(self, player):
      raise NotImplementedError("This method must be implemented by an inherited class.")

class FavoriteStrategy(StrategyBase):
   """Implements the 'favorite' strategy.

   The 'favorite' strategy is used to compute a move
   that beats the players most frequent move.
   """
   
   def compute_turn(self, player):

      most_frequent_move = player.most_frequent_move

      return get_superior_move(most_frequent_move)

class LastMoveStrategy(StrategyBase):
   """Implements the 'last move' strategy.

   The 'last move' strategy is used to compuet a move
   that beats the players last move.
   """

   def compute_turn(self, player):

      last_move = player.last_move

      return get_superior_move(last_move)

class RandomMoveStrategy(StrategyBase):
   """Implements the 'random move' strategy.

   The random move strategy is simple, each move has an 
   equal chance of being played.
   """

   def __init__(self, seed_value = None):
      self.seed_value = seed_value

   def compute_turn(self, player):
      """Computes a random move to be made.

      If a seed is provided, the same move will be made every call
      as long as the seed remains the same, otherwise the system
      time will be used. The seed is mainly for testing purposes.
      """

      from random import sample, seed
      from .moves import Move

      seed(a = self.seed_value)

      move = sample([x[1] for x in Move.__members__.items()], 1)[0]

      return move

def strategy_factory(strategy_name):
   """Returns a strategy from the provided string value.
   """

   if strategy_name not in available_types:
      return None

   if strategy_name == available_types[0]:
      return FavoriteStrategy()
   elif strategy_name == available_types[1]:
      return LastMoveStrategy()
   elif strategy_name == available_types[2]:
      return RandomMoveStrategy()
