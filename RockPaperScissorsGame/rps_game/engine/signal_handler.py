import signal, sys

def signal_handler(signal, frame):
   """Catches a Ctrl+C input to gracefully end the process.
   """
   print("Caught Ctrl+C...")
   print("Thanks for playing. Goodbye!")

   sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)