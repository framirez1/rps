
import argparse, sys
import rps_game.engine.signal_handler

if sys.version_info[0] == 3:
   from rps_game.engine.rps_game import RpsGame
   from rps_game.engine.strategies import available_types, strategy_factory

def main():

   if sys.version_info[0] != 3:
      print("This game requires Python 3 to run.")
      return 1

   parser = argparse.ArgumentParser(prog="rps")

   parser.add_argument('-s', '--strategy', type=str, required=True, choices=available_types,
                        help = "Run the game with the specified strategy.")
   
   args = parser.parse_args()

   game = RpsGame()

   game.start(strategy = strategy_factory(args.strategy))

if __name__ == "__main__":
   sys.exit(int(main() or 0))